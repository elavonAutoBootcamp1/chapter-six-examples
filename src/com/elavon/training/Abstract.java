package com.elavon.training;

import com.elavon.training.Static.MyIntstanceClass;

public class Abstract {

	public static void main(String[] args) {
		Animal2 absClass = new Bird();
		absClass.method1();
	}

}

class Bird extends Animal2 {
	@Override
	void eat() {
		System.out.println("make tuka");
	}
	
	void method2(){
		System.out.println("called from MyConcreteClass");
	}

	@Override
	void makeSOund() {
		// TODO Auto-generated method stub
		
	}
}

class Dog2 extends Animal2 {
	@Override
	void eat() {
		System.out.println("Dog eats");
	}
	@Override
	void makeSOund() {
		System.out.println("BarK!");		
	}
}

abstract class Animal2 {
	
	abstract void eat();
	abstract void makeSOund();
	
	void method1(){
		System.out.println("called from method1()");
	}
	
}

