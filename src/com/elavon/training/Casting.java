package com.elavon.training;

public class Casting {

	private static void runtimeTimeError() {
		Animal animal = new Dog();
		Pug pug = (Pug) animal;
	}
	
	
	public static void main(String[] args) {
		runtimeTimeError();
		
		Animal animal1 = new Dog();
		animal1.animalMethod();
		Dog pug = (Dog) animal1; // downcasting

		Animal animal2 = new Pug();
		Dog pug2 = (Dog) animal2; // downcasting
		
		System.out.println("Pug2: "+pug2);	//error

		Animal animal = new Bengal();
		animal.animalMethod();
		
		Cat cat = (Cat) animal; // upcasting
		
//		Cat cat3 = (Cat) new Pug(); //compile time error
		
		cat.catMethod();
		cat.animalMethod();
		
//		Dog d = new Dog();
//		Cat cat2 = (Cat) d;	//error
		
		System.out.println(cat);
	}

}

class Animal {
	void animalMethod(){
		System.out.println("from animalMethod()");
	}
}

class Dog extends Animal {
	void dogMethod(){}
//	@Override
//	void animalMethod(){
//		System.out.println("Dog's implementation of animalMethod()");
//	}
}

class Cat extends Animal {
	void catMethod(){}
}

class Pug extends Dog {
}

class Hound extends Dog {
}

class Bengal extends Cat {
}
