package com.elavon.training;

class Doggy {
	String name;
	int id = 0;
	
	public Doggy(String name, int id) {
		this.name = name;
		this.id = id;
	}

	@Override
	public boolean equals(Object d2) {

		if (d2 instanceof Doggy) {
			Doggy d = (Doggy) d2;
			if (this.name.equals(d.name) && this.id == d.id) {
				return true;
			}
		}
		return false;
	}
}

public class String_ {

	public static void main(String[] args) {
		eqs();
	}

	private static void eqs() {
		Doggy d1 = new Doggy("John", 1);
		Doggy d2 = new Doggy("John", 2);

		System.out.println(d1.equals(d2));

	}

	private void stringbuff() {
		StringBuffer sbf = new StringBuffer();
		sbf.append("").append(1).append('c').append(new Object());

		System.out.println(sbf);

	}

	private void stringBuilderExecise() {
		StringBuilder sb = new StringBuilder();

		sb.append("111").append("222").append("333").append("444");

		StringBuilder sb2 = new StringBuilder();

		sb2.append("aaa");
		sb2.append("bbb");
		sb2.append("ccc");

		sb.append(sb2);

		System.out.println(sb);
	}

	private static void stringExercise() {
		String str = "1232425";

		String newStr = str.replace("2", "");

		System.out.println("str: " + str);

		System.out.println(newStr.equals("1345"));

		System.out.println("1345".equals(newStr)); // best practice

		System.out.println("newStr: " + newStr);

		String s2 = "2";
		String s1 = "1" + s2;

		System.out.println(s1 == "12");
	}

}
