package com.elavon.training;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpression {

	public static void main(String[] args) {
		
//		System.out.println(Pattern.matches("\\d* ", "4 "));
		String input = "qwetyy 123 wqe 4234 asjkdzh $573.45 $573t45";
		
		Matcher matcher = Pattern.compile("(asjkdzh) ([$]\\d+[.]\\d+)").matcher(input);
		
		while(matcher.find()){
//			System.out.print("start = " + matcher.start());
//			System.out.println(" end = " + matcher.end());
			System.out.println(matcher.group(0));
			System.out.println(matcher.group(1));
		}
		
	}
	
	public static void simpleRegex(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Maria ate 34 apples");
		System.out.println("John ate 7 apples");
		System.out.println("Daisy ate 2 apples");
		System.out.println("Daisy ate  apples");
		
		
		System.out.println("ra7nd6om3 9s4tri1ng1".replaceAll("\\d", ""));
		
	}

}
