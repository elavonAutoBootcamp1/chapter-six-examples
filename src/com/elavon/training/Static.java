package com.elavon.training;

public class Static {

	static int COUNTER; // class variable

	int instanceInt;
	
	public Static() {
		COUNTER++;
		instanceInt++;
	}
	static {// static block
//		System.out.println("from static block");
	}

	{
//		System.out.println("instance block");
		
	}
	public static void main(String[] args) {// static method
		Static s1 = new Static();
		Static s2 = new Static();
		Static s3 = new Static();
		Static s4 = new Static();
		
//		System.out.println(Static.COUNTER);	//4
//		System.out.println(s4.instanceInt); //1
		
		Static s5 = null;
		System.out.println(s5.COUNTER); //4
		System.out.println(s5.instanceInt); //nullpointerexception
		
		
		MyStaticClass myStaticClass = new Static.MyStaticClass();
		MyIntstanceClass myIntstanceClass = s3.new MyIntstanceClass();
	}
	
	void instanceMethod(){
		COUNTER++;
	}

	static class MyStaticClass {// static nested class
	}
	
	class MyIntstanceClass{}

}
