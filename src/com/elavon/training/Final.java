package com.elavon.training;

class Dog3 {
	String name =  "John Doe";
	
	final void bark(){
		System.out.println("Bark");
	}
}

class NewDog extends Dog3 {
//	/void bark(){} //error
}
public class Final {

	public static void main(final String[] args) {
//		args[0] = "3";
		final Dog3 dog = new Dog3();
		
		dog.name = "Jane Doe";
		int x = 9;
		printMe(dog, x);
		
		System.out.println("x: "+x);
		System.out.println("name after call: "+dog.name);
		i++;
	}
	static int i;
	private static void printMe(final Dog3 dog1, int num){
		num = 11;
		dog1.name = "new name";
		System.out.println(dog1.name);
	}

}
