package com.elavon.training;

public interface Printable {
	public static final int copies = 0;
	double d = 0;
	
	public abstract void print();
	
}
