package com.elavon.training;

public class Excepts {

	public static void main(String[] args) {
//		try {
//			exceptionThrower(-9);
//		} catch (NegativeNumberException e) {
//			e.printStackTrace();
//		}
		numFOrmat("123a");	//numberformatException
	}
	
	private static void numFOrmat(String num) {
		
		int i = Integer.parseInt(num);
		System.out.println("i "+(i+1));

	}

	static void exceptionThrower(int posNum) throws NegativeNumberException {
		
		if(posNum < 0){
			throw new NegativeNumberException();
		}
	}
	
	private void divByZero() {
			for (int i = 0; i < 5; i++) {
				System.out.println(9 / i);	//runtime exception
			}

		System.out.println("PROGRAM ENDS");

	}

}

class NegativeNumberException extends Exception {
	
}
